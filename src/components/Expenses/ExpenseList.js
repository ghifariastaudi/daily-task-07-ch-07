import React from 'react';
import './ExpenseList.css';
import ExpenseItem from './ExpenseItem';

const ExpenseList = props => {
    // let expenseContent = <p>No Expense Found</p>
    if(props.items.length === 0) {
    return <p>No Expense Found</p>
  }
  return (
    <ul className="expenses-list">
      {props.items.map((expense) => (
      <ExpenseItem
        key={expense.id}
        title={expense.title}
        amount={expense.amount}
        date={expense.date}
      />
    ))}
    </ul>
  )
}

export default ExpenseList;